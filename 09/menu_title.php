<!DOCTYPE html><!--ソースを記載するときに必要なコード-->
<?php
  include("dbsys.php"); //DB接続情報を別にする

  $where_str = ""; //検索
  $query_str = "SELECT * FROM test_table";

  if (!empty($_GET['id'])) {  //idが空であることを確認する
    $where_str .= " AND id = '". $_GET['id'] . "'";
   }
  if (!empty($_GET['name'])) {
    $where_str .= " AND name = '" . $_GET['name'] . "'";
    // $cond_dishname = $_GET['name'];
  }
  if (!empty($_GET['type'])) {
    $where_str .= " AND type = '" . $_GET['type'] . "'";
    // $cond_genre = $_GET['type'];
  }
  if (!empty($_GET['price'])) {
    $where_str .= " AND price = '" . $_GET['price'] . "'";
  }
  // echo $where_str;
  // // Where文以降を生成する。

  // SQLの基本部分。全権検索のときはこれだけ。
  $query_str .= " WHERE 1 = 1 ".$where_str;

  echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
 ?>

<html> <!-- //htmlの宣言 -->
  <head> <!-- //文書のヘッダ情報始め -->
    <meta charset="utf-8"> <!-- //ページの情報を定義する -->
    <meta name="viewport" content="width=device-width, initial-scale = 1"> <!-- //画面を表示させる時のコード -->
    <title>開発研修居酒屋料理メニュー</title> <!-- //ページタイトル -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <!-- 画像のサンプルコードが豊富に格納されているサイト名 -->
     <script type="text/javascript">
      function resetForms(){
       // 名前の空欄判定
      document.dish_search.id.value = "";
      document.dish_search.name.value = "";
      document.dish_search.tape.value = "";
      document.dish_search.price.value = "";
      }
    </script>
  </head> <!-- //文書のヘッダ情報終わり -->
    <body>
     <h1><center>開発研修居酒屋料理メニュー</center></h1>
     <!-- ヘッダー部のタイトル名 -->
     <!-- h1 = 見出し要素-->
     <!-- center = 表示を真ん中に表示させる -->
     <form method="get" name="menu_title.php"> <!-- menu_title.phpにデータを送信する -->
      <center>商品名：<input type="text" name="name"></center> <!-- 入力テキストタイプ -->
      <center>ジャンル：<input type="text" name="type"></center> <!-- 入力テキストタイプ -->
      <center>値段：<input type="text" name="price"></center> <!-- 入力テキストタイプ -->
    <div style = "text-align:center">
     <input type="submit" value="検索">
     <input type="reset" value="リセット">
    </div>
  <hr>
   <table class="table table-striped">
     <th colspan="1">商品名</th>
     <th>ジャンル</th>
     <th>お値段</th>
     <th>メモ</th>
  <?php foreach($result as $each){ ?>
    <?php // var_dump($each); ?>
      <tr>
      <td><a href ="menu_details.php?id=<?=$each['id']?>"><?=$each['name']?></a></td>
      <td><?=$each['type'] ?></td>
      <td><?=number_format($each['price'])?>円</td>
      <td><?=$each['memo']?></td>
      </tr>
   <?php } ?>
  </table>
 </body>
</html>
