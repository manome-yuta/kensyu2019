<!DOCTYPE html><!--ソースを記載するときに必要なコード-->
<?php
  include("dbsys.php"); //DB接続情報を別にする

  $where_str = "";
  $query_str = "SELECT * FROM test_table ";   // SQLの基本部分。全権検索文。

  //DBからidを検索するSQL文
  if (!empty($_GET['id'])) {
  $where_str .= "id = '" . $_GET['id'] . "' AND";
  }
  if($where_str != ""){
    // WHEREを頭につけて、語尾の4文字を削除する。
  $where_str = " WHERE " . substr($where_str, 0, -4);
  }
  $query_str .= $where_str;

  echo $query_str; //ブラウザにSQL分を表示させるプログラム文
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
 ?>
<!-- 検索結果が1件じゃないときは、エラーを出す必要。 -->

<html> <!-- htmlの宣言 -->
  <head> <!-- 文書のヘッダ情報始め -->
    <meta charset="utf-8"> <!-- 文字コードの情報を定義する -->
    <meta name="viewport" content="width=device-width, initial-scale = 1"> <!-- 画面の横幅を指定するコード -->
    <title>開発研修詳細画面</title> <!-- ページタイトル -->
    <link rel="stylesheet" href="style.css"> <!-- スタイルシート（外部CSSファイル）を読み込む -->
  </head> <!-- 文書のヘッダ情報終わり -->
<body>

<?php foreach($result as $each){
  } ?>

 <h1><left>商品名詳細画面</left></h1>
   <table border="1" align="center">
    <tr>
     <td>ID</td><td><?=$each['id']?></td>
    </tr>
    <tr>
     <td colspan="1">商品名</td><td><?=$each['name']?></td>
    </tr>
    <tr>
     <td>ジャンル</td><td><?=$each['type']?></td>
    </tr>
     <td>お値段</td><td><?=number_format($each['price'])?>円</td>
    <tr>
     <td>メモ</td><td><?=$each['memo']?></td>
   </tr>
  </table>
</br>
  <center><button type="button" onclick="history.back()">戻る</button></center>
 </body>
</html>
