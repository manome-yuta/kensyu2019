<!DOCTYPE html><!--ソースを記載するときに必要なコード-->
<?php
  include("../include/indexdb.php");
  include("../include/prefec.php"); //DB接続情報を別にする
  $pdo = initDB();

  if(isset($_GET['id'])) {
  $user_id = $_GET['id'];
  }

  if(isset($_GET['id'])) {
  $user_id != $_GET['id'];
  }

  $sql  = "                                                                   ";
  $sql .= "    SELECT                                                         ";
  $sql .= "    tbl_user.id,                                                   ";
  $sql .= "    tbl_user.name,                                                 ";
  $sql .= "    tbl_user.sex,                                                  ";
  $sql .= "    tbl_user.prefectures,                                          ";
  $sql .= "    tbl_user.age,                                                  ";
  $sql .= "    mst_department.department,                                     ";
  $sql .= "    mst_post.post_name                                             ";
  $sql .= "    FROM                                                           ";
  $sql .= "    tbl_user                                                       ";
  $sql .= "    LEFT JOIN                                                      ";
  $sql .= "    mst_department                                                 ";
  $sql .= "    ON  mst_department.id = tbl_user.depart                        ";
  $sql .= "    LEFT JOIN                                                      ";
  $sql .= "    mst_post                                                       ";
  $sql .= "    ON  mst_post.id = tbl_user.post                                ";
  $sql .= "    WHERE                                                          ";
  $sql .= "    tbl_user.id = '".$user_id."'                                   ";

  echo $sql;
  $sql = $pdo->prepare($sql);
  $sql->execute();
  $result = $sql->fetchAll();
 ?>

<html> <!-- htmlの宣言 -->
  <head> <!-- 文書のヘッダ情報始め -->
      <meta charset="utf-8"> <!-- //ページの情報を定義する -->
      <meta name="viewport" content="width=device-width, initial-scale = 1"> <!-- //画面を表示させる時のコード -->
      <title>開発研修詳細画面</title> <!-- ページタイトル -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

  </head> <!-- 文書のヘッダ情報終わり -->
    <body>
     <div class="container-fluid">
      <div class="row mb-2">
       <div class="col-md-12">
        <h1 align="light">社員詳細画面</h1>
         <a href="index.php" style="float:rigth;">トップ画面へ戻る|</a>
         <script type="text/javascript">
           function goDel(id){
            if(window.confirm('削除を行います。よろしいですか？')){
             location.href = "delete.php?id=" + id;
              }
             }
           function goEdit(id){
             location.href = "update.php?id=" + id;
              }

         </script>
          <div class="row justify-content-center">
            <div class="col-md-3">
              <table class="table table-striped table-bordered text-nowrap">
                <td>社員ID</td><td><?php echo $result[0]['id'];?></td>
                  <tr>
                    <td>名前</td><td><?php echo $result[0]['name'];?></td>
                  </tr>
                  <tr>
                    <td>出身地</td><td><?php echo $prefec_array[$result[0]['prefectures']];?></td>
                  </tr>
                    <td>性別</td><td><?php echo $sex_array[$result[0]['sex']];?></td>
                  <tr>
                    <td>年齢</td><td><?php echo $result[0]['age'];?></td>
                  </tr>
                  <tr>
                    <td>所属部署</td><td><?php echo $result[0]['department'];?></td>
                  </tr>
                  <tr>
                    <td>役職</td><td><?php echo $result[0]['post_name'];?></td>
                  </tr>
              </table>
                <div style = "text-align:center">
                  <input type="button" class="btn btn-info" value="編集" onclick="goEdit(<?php echo $result[0]['id']; ?>);">
                  <input type="button" class="btn btn-secondary"value="削除" onclick="goDel(<?php echo $result[0]['id']; ?>);">
                </div>
            </div>
          </div>
       </div>
     </div>
  </body>
</html>
