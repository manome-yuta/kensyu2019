
<!DOCTYPE html>
<?php
include("../include/indexdb.php");
include("../include/prefec.php");
$pdo = initDB();
?>

<html> <!-- htmlの宣言 -->
  <head> <!-- 文書のヘッダ情報始め -->
    <meta charset="utf-8"> <!-- //ページの情報を定義する -->
    <meta name="viewport" content="width=device-width, initial-scale = 1"> <!-- //画面を表示させる時のコード -->
    <title>開発研修詳細画面</title> <!-- ページタイトル -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

  </head> <!-- 文書のヘッダ情報終わり -->
    <body>
      <div class="container-fluid">
      <div class="row mb-4">
      <div class="col-md-12">
       <h1 align="light">新規登録画面</h1>
        <td class="ta_rt" id="header_menu">|<a href="index.php">トップ画面</a> | <a href="entry01.php">新規社員登録へ</a> | </td>

              <script type="text/javascript">
              <!--
               function conf(){
                // 名前のバリデーション
                // var temp = document.forms.update.name01.value.replace(/\s+/g, "");
                // if( temp == "" ){
                //   alert('名前(漢字)を入力してください。');
                //   return false;
                // }
                // document.forms.update.name01.value = temp;
                //
                // // 出身地のバリデーション
                // if( document.forms.update.pref.value == "" ){
                //   alert('出身地を入力してください。');
                //   return false;
                // }
                //
                // // 年齢のバリデーション
                // temp = document.forms.update.age01.value;
                // if( temp == "" ){
                //   alert('年齢を入力してください。');
                //   return false;
                // }
                //
                // if(isNaN(temp) || temp <= 0 || temp > 99){
                //   alert('・年齢は数字で入力してください。\n・0～99の範囲で入力してください。');
                //   return false;
                // }

                if(window.confirm('登録を行います。よろしいですか？')){
                  document.update.submit();
                } else {
                }
              }
              </script>

    <body>
    <hr>
    <form method="POST" action="entry02.php" name="update">
      <div class="row justify-content-center">
       <div class="col-md-4">
        <div class="result_wrap detail_result" id="tbl-bdr">
         <table class="table table-striped table-bordered text-nowrap">
          <tr>
           <th>名前</th>
           <td><input class="form-control" type="text" maxlength="30" name="name" required></td>
          </tr>
          <tr>
            <th>出身地</th>
            <td>
              <div class="form-group">
                <select name="prefectures" id="select1a" class="form-control" required>
                  <?php
                   foreach($prefec_array as $key => $value){
                   echo "<option value='" . $key . "'>" . $value . "</option>";
                 }
                    ?>
                </select>
              </div>
            </td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sex" id="men" value="1" required>
                <label class="form-check-label" for="men">男性</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sex" id="woman" value="0" required>
                <label class="form-check-label" for="woman">女性</label>
              </div>
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><input class="form-control" type="number" name="age" min="18" max="100" required></td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="department" id="prefec1" value="1" required>
                <label class="form-check-label" for="prefec1">第一事業部</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="department" id="prefec2" value="2" required>
                <label class="form-check-label" for="prefec2">第二事業部</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="department" id="prefec3" value="3" required>
                <label class="form-check-label" for="prefec3">営業</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="department" id="prefec4" value="4" required>
                <label class="form-check-label" for="prefec4">総務</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="department" id="prefec5" value="5" required>
                <label class="form-check-label" for="prefec5">人事</label>
              </div>
            </td>
          </tr>
          <tr>
            <th>役職</th>
            <td>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="post" id="post1" value="1" required>
                <label class="form-check-label" for="post1">事業部長</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="post" id="post2" value="2" required>
                <label class="form-check-label" for="post2">部長</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="post" id="post3" value="3" required>
                <label class="form-check-label" for="post3">チームリーダー</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="post" id="post4" value="4" required>
                <label class="form-check-label" for="post4">リーダー</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="post" id="post5" value="5" required>
                <label class="form-check-label" for="post5">メンバー</label>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="ta_rt">
        <input type="submit" class="btn btn-info" value="登録" onclick="conf();">
        <input type="reset" class="btn btn-secondary" value="リセット">
        <input type="hidden" value="2" name="member_ID">
      </div>
    </form>
  </body>
</html>
