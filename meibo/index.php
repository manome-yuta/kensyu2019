<!DOCTYPE html><!--ソースを記載するときに必要なコード-->
<?php
  include("../include/indexdb.php"); //DB接続情報を別にする
  $pdo = initDB();

  $where_str  = "";
  $name       = "";
  $sex        = "";
  $department = "";
  $post       = "";

  $sql  = "                                                                   ";
  $sql .= "    SELECT                                                         ";
  $sql .= "    tbl_user.id,                                                   ";
  $sql .= "    tbl_user.name,                                                 ";
  $sql .= "    tbl_user.sex,                                                  ";
  $sql .= "    tbl_user.prefectures,                                          ";
  $sql .= "    tbl_user.age,                                                  ";
  $sql .= "    mst_department.department,                                     ";
  $sql .= "    mst_post.post_name                                             ";
  $sql .= "    FROM                                                           ";
  $sql .= "    tbl_user                                                       ";
  $sql .= "    LEFT JOIN                                                      ";
  $sql .= "    mst_department                                                 ";
  $sql .= "    ON  mst_department.id = tbl_user.depart                        ";
  $sql .= "    LEFT JOIN                                                      ";
  $sql .= "    mst_post                                                       ";
  $sql .= "    ON  mst_post.id = tbl_user.post                                ";
  $sql .= "    WHERE 1 = 1                                                    ";

  if (isset($_GET['name']) && !empty($_GET['name'])) {
  $sql .= " AND tbl_user.name LIKE '%" . $_GET['name'] . "%'                  ";
  $name       = $_GET['name'];
  }

  if (isset($_GET['sex']) && ($_GET['sex'] != ""))  {
  $sql .= " AND tbl_user.sex = '" . $_GET['sex'] . "'                         ";
  $sex        = $_GET['sex'];
  }

  if (isset($_GET['department']) && !empty($_GET['department'])){
  $sql .= " AND tbl_user.depart = '" . $_GET['department'] . "'               ";
  $department = $_GET['department'];
  }

  if (isset($_GET['post']) && !empty($_GET['post'])) {
  $sql .= " AND tbl_user.post = '" . $_GET['post'] . "'                       ";
  $post       = $_GET['post'];
  }

  $sql .= "    ORDER BY id                                                    ";
  echo $sql;

  $sql = $pdo->prepare($sql);
  $sql->execute();
  $result = $sql->fetchAll();
  // var_dump($result);
 ?>

<html> <!-- //htmlの宣言 -->
  <head> <!-- //文書のヘッダ情報始め -->
    <meta charset="utf-8"> <!-- //ページの情報を定義する -->
    <meta name="viewport" content="width=device-width, initial-scale = 1"> <!-- //画面を表示させる時のコード -->
    <title>社員名簿システム</title> <!-- //ページタイトル -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

  </head> <!-- //文書のヘッダ情報終わり -->
    <body>
      <div class="container-fluid">
        <div class="row mb-4">
         <div class="col-md-12">
            <h1 align="light">社員名簿システム</h1>
              <a href="index.php" style="float:rigth;">トップ画面</a> | <a href="entry01.php">新規社員登録</a>

                <form method="get" action="index.php">
                  <center>名前：<input type="text" name="name" value ="<?php echo $name;?>"></center>
                    <center>
                      性別：<select name="sex">
                        <option value=""  <?php if($sex == ""  ){echo "selected";}?>>全て</option>
                        <option value="0" <?php if($sex == "0" ){echo "selected";}?>>女性</option>
                        <option value="1" <?php if($sex == "1" ){echo "selected";}?>>男性</option>
                            </select>
                      &ensp;
                      部署：<select name="department">
                        <option value="" <?php if($department == ""  ){echo "selected";}?>>全て</option>
                        <option value="1"<?php if($department == "1" ){echo "selected";}?>>第一事業部</option>
                        <option value="2"<?php if($department == "2" ){echo "selected";}?>>第二事業部</option>
                        <option value="3"<?php if($department == "3" ){echo "selected";}?>>営業</option>
                        <option value="4"<?php if($department == "4" ){echo "selected";}?>>総務</option>
                        <option value="5"<?php if($department == "5" ){echo "selected";}?>>人事</option>
                           </select>
                      &ensp;
                      役職：<select name="post">
                        <option value="" <?php if($post == ""  ){echo "selected";}?>>全て</option>
                        <option value="1"<?php if($post == "1" ){echo "selected";}?>>事業部長</option>
                        <option value="2"<?php if($post == "2" ){echo "selected";}?>>部長</option>
                        <option value="3"<?php if($post == "3" ){echo "selected";}?>>チームリーダー</option>
                        <option value="4"<?php if($post == "4" ){echo "selected";}?>>リーダー</option>
                        <option value="5"<?php if($post == "5" ){echo "selected";}?>>メンバー</option>
                           </select>
                    </center>
<br>
          <div style = "text-align:center">
            <button type="submit" class="btn btn-info">検索</button>
            <button type="reset" class="btn btn-secondary">リセット</button>
          </div>
         </div>
        </div>
      </div>
<hr>
     <div class="row justify-content-center">
      <div class="col-md-4">
       <table class="table table-striped table-bordered text-nowrap">
        <thead class="thead-dark">
         <tr>
           <th>社員ID</th>
           <th>名前</th>
           <th>部署</th>
           <th>役職</th>
         </tr>
        </thead>
         <tbody>
          <?php foreach($result as $each){ ?>
           <tr>
            <td><?=$each['id']?></td>
            <td><a href ="detail.php?id=<?=$each['id']?>"><?=$each['name']?></a></td>
            <td><?=$each['department'] ?></td>
            <td><?=$each['post_name']?></td>
           </tr>
          <?php } ?>
         </tbody>
       </table>
      </div>
     </div>
   </body>
</html>
