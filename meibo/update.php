<!DOCTYPE html>
<?php
include("../include/indexdb.php");
include("../include/prefec.php");
$pdo = initDB();

$sql  = "                                                                   ";
$sql .= "    SELECT                                                         ";
$sql .= "    tbl_user.id,                                                   ";
$sql .= "    tbl_user.name,                                                 ";
$sql .= "    tbl_user.sex,                                                  ";
$sql .= "    tbl_user.prefectures,                                          ";
$sql .= "    tbl_user.age,                                                  ";
$sql .= "    tbl_user.depart,                                               ";
$sql .= "    tbl_user.post,                                                 ";
$sql .= "    mst_department.department,                                     ";
$sql .= "    mst_post.post_name                                             ";
$sql .= "    FROM                                                           ";
$sql .= "    tbl_user                                                       ";
$sql .= "    LEFT JOIN                                                      ";
$sql .= "    mst_department                                                 ";
$sql .= "    ON  mst_department.id = tbl_user.depart                        ";
$sql .= "    LEFT JOIN                                                      ";
$sql .= "    mst_post                                                       ";
$sql .= "    ON  mst_post.id = tbl_user.post                                ";
$sql .= "    WHERE                                                          ";
$sql .= "    tbl_user.id = '". $_GET['id']."'                               ";

echo $sql;
$sql = $pdo->prepare($sql);
$sql->execute();
$result = $sql->fetchAll();
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>編集ページ</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
  </head>
  <script type="text/javascript">
  <!--
   function conf(){
    // // 名前のバリデーション
    // var temp = document.forms.update.name01.value.replace(/\s+/g, "");
    // if( temp == "" ){
    //   alert('名前(漢字)を入力してください。');
    //   return false;
    // }
    // document.forms.update.name01.value = temp;
    //
    //
    // // 出身地のバリデーション
    // if( document.forms.update.pref.value == "" ){
    //   alert('出身地を入力してください。');
    //   return false;
    // }
    //
    // // 年齢のバリデーション
    // temp = document.forms.update.age01.value;
    // if( temp == "" ){
    //   alert('年齢を入力してください。');
    //   return false;
    // }
    //
    // if(isNaN(temp) || temp <= 0 || temp > 99){
    //   alert('・年齢は数字で入力してください。\n・0～99の範囲で入力してください。');
    //   return false;
    // }

    if(window.confirm('更新を行います。よろしいですか？')){
      document.update.submit();
      // alert("true");
    } else {
      // alert("false");
    }
  }
  -->
  </script>
  <body>
    <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-md-12">
    <h1 align="light">社員名簿システム</h1>
    <td class="ta_rt" id="header_menu">|<a href="index.php">トップ画面</a> | <a href="entry01.php">新規社員登録へ</a> | </td>
</table>
    <hr>
    <form method="POST" action="update02.php" name="update">
      <div class="row justify-content-center">
       <div class="col-md-4">
        <div class="result_wrap detail_result" id="tbl-bdr">
         <table class="table table-striped table-bordered text-nowrap">
          <tr>
            <th>社員ID</th>
            <td><?php echo $result[0]['id'];?></td>
          </tr>
          <tr>
            <th>名前</th>
            <td><input type="text" name="name" value="<?php echo $result[0]['name'];?>" required></td>
          </tr>
          <tr>
            <th>出身地</th>
            <td>
              <select name="pref" required>
                <?php
                 foreach($prefec_array as $key => $value){
                 echo "<option value='" . $key . "'";
                 if($result[0]['prefectures'] == $key){ echo " selected ";}
                 echo ">" . $value . "</option>";
                 }
                 ?>
              </select>
            </td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
              <input type='radio' name='sex' <?php if ($result[0]['sex'] == "1") { echo "checked"; } ?> value='1'>男性
              <input type='radio' name='sex' <?php if ($result[0]['sex'] == "0") { echo "checked"; } ?> value='0'>女性
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><input type="number" name="age" value="<?php echo $result[0]['age'];?>" max="100" min="18" required></td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td>
              <?php
                foreach(getdepartment() as $each){
                 echo "<input type='radio' id='department" . $each['id']
                  . "' name='department' value='" . $each['id'] . "'";
                 if($each['id'] == $result[0]['depart']) echo " checked ";
                 echo "><label for='sec" . $each['id'] . "'>" . $each['department'] . "</label>";
                }
              ?>
            </td>
          </tr>
          <tr>
            <th>役職</th>
            <td>
              <?php
                foreach(getPost() as $each){
                echo "<input type='radio' id='post" . $each['id']
                . "' name='post' value='" . $each['id'] . "'";
                if($each['id'] == $result[0]['post']) echo " checked ";
                echo "><label for='post" . $each['id'] . "'>" . $each['post_name'] . "</label>";
                }
              ?>
            </td>
          </tr>
        </table>
      </div>
      <div class="ta_rt">
        <input type="button" class="btn btn-info" value="更新" onclick="conf();">
        <input type="reset" class="btn btn-secondary" value="リセット">
        <input type="hidden" value="2" name="id">
      </div>
      </div>
      </div>
      </div>
    </form>
  </body>
</html>
