<!DOCTYPE html><!--ソースを記載するときに必要なコード-->
<html> <!-- //htmlの宣言 -->
  <head> <!-- //文書のヘッダ情報始め -->
    <meta charset="utf-8"> <!-- //ページの情報を定義する -->
    <meta name="viewport" content="width = device-width, initial-scale = 1"> <!-- //画面を表示させる時のコード -->
    <title>行追加</title> <!-- //ページタイトル -->
    <link rel="stylesheet" href="style.css"> <!-- //スタイルシート（外部CSSファイル）を読み込む -->
  </head> <!-- //文書のヘッダ情報終わり -->
  <body>
    <form method="get" action="tabel_rennzoku2.php"> <!-- 06_tabel_rennzoku.phpにデータを送信する -->
        <p>料理名：<input type="text" name="cols"> <!-- 入力テキストタイプで -->
        <input type="text" name="rows">
        <br>列のテーブルを生成する
<br>
        <input type="submit"> <!-- 実行ボタン -->
        <input type="reset"> <!-- リセットボタン -->
    </form>
    <hr>
    <table border="1" style="border-collapse: collapse">
      <?php
        if(isset($_GET['cols'])){
          for($i = 0; $i < $_GET['cols']; $i++){
            echo "<tr>";
            if(isset($_GET['rows'])){
              for($a = 0; $a < $_GET['rows']; $a++){
                echo "<td>" . ($i+1) . "-" . ($a+1) . "</td>";
              }
              echo "</tr>";
            }
          }
        }
          ?>
     </table>
    </body>
</html>
