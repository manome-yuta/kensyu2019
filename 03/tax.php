<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>開発研修宿題（消費税計算ページ）</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h2>開発研修宿題（消費税計算ページ）</h2>
　　<table border="1">
　　　<tr>
        <th>商品名</th>
        <th>価格（単位：円、税抜き）</th>
        <th>個数</th>
        <th>税率</th>
      </tr>
      　<form method="get" action="tax.php">
    　<tr>
       <td>
      <input type="text" name="productname_1">
       </td>
       <td>
        <input type="text" name="price_1">
       </td>
       <td>
        <input type="text" name="number_1">個
       </td>
       <td>
        <input type="radio" name="tax_1" value="0.08">0.8%
        <input type="radio" name="tax_1" value="0.10">10.0%
       </td>
     </tr>
     <tr>
       <td>
        <input type="text" name="productname_2">
       </td>
       <td>
        <input type="text" name="price_2">
       </td>
       <td>
        <input type="text" name="number_2">個
       </td>
       <td>
        <input type="radio" name="tax_2" value="0.08">0.8%
        <input type="radio" name="tax_2" value="0.10">10.0%
       </td>
     </tr>
     <tr>
       <td>
        <input type="text" name="productname_3">
       </td>
       <td>
        <input type="text" name="price_3">
       </td>
       <td>
        <input type="text" name="number_3">個
       </td>
       <td>
        <input type="radio" name="tax_3" value="0.08">0.8%
        <input type="radio" name="tax_3" value="0.10">10.0%
       </td>
     </tr>
     <tr>
       <td>
        <input type="text" name="productname_4">
       </td>
       <td>
        <input type="text" name="price_4">
       </td>
       <td>
        <input type="text" name="number_4">個
       </td>
       <td>
        <input type="radio" name="tax_4" value="0.08">0.8%
        <input type="radio" name="tax_4" value="0.10">10.0%
       </td>
     </tr>
     <tr>
       <td>
        <input type="text" name="productname_5">
       </td>
       <td>
        <input type="text" name="price_5">
       </td>
       <td>
        <input type="text" name="number_5">個
       </td>
       <td>
        <input type="radio" name="tax_5" value="0.08">0.8%
        <input type="radio" name="tax_5" value="0.10">10.0%
       </td>
     </tr>
   </table>
        <input type="submit">
        <input type="reset">
       </form>
       <hr>
   <table border="1">
     <tr>
       <th>商品名</th>
       <th>価格（単位：円、税抜き）</th>
       <th>個数</th>
       <th>税率</th>
       <th>小計</th>
   　</tr>
     <tr>
       <td>
        <?php
        if(isset($_GET['productname_1'])){
        echo $productname_1 = $_GET['productname_1'];
         }
         ?>&nbsp;
       </td>
       <td>
         <?php
         if(isset($_GET['price_1'])){
           echo $price_1 = $_GET['price_1'];
           echo "円（税抜き）";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['number_1'])){
           echo $number_1 = $_GET['number_1'];
           echo "個";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['tax_1'])){
          echo $tax_1 = $_GET['tax_1'];
         }
           ?>
       </td>
       <td>
         <?php
         if(isset($_GET['price_1'],$_GET['number_1'],$_GET['tax_1'])){
         $nedan_1 = $_GET['price_1'] * $_GET['number_1'] * $_GET['tax_1'];
         echo $nedan_1;
         echo "円（税込み）";
         }
         ?>
       </td>
     </tr>
     <tr>
       <td>
         <?php
         if(isset($_GET['productname_2'])){
           echo $productname_1 = $_GET['productname_2'];
         }
         ?>&nbsp;
       </td>
       <td>
         <?php
         if(isset($_GET['price_2'])){
           echo $price_1 = $_GET['price_2'];
           echo "円（税抜き）";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['number_2'])){
           echo $number_1 = $_GET['number_2'];
           echo "個";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['tax_2'])){
          echo $tax_1 = $_GET['tax_2'];
         }
           ?>
       </td>
       <td>
         <?php
         if(isset($_GET['price_2'],$_GET['number_2'],$_GET['tax_2'])){
         $nedan_2 = $_GET['price_2'] * $_GET['number_2'] * $_GET['tax_2'];
         echo $nedan_2;
         echo "円（税込み）";
         }
         ?>
       </td>
     </tr>
     <tr>
       <td>
         <?php
         if(isset($_GET['productname_3'])){
         echo $productname_1 = $_GET['productname_3'];
         }
         ?>&nbsp;
       </td>
       <td>
         <?php
         if(isset($_GET['price_3'])){
          echo $price_3 = $_GET['price_3'];
          echo "円（税抜き）";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['number_3'])){
          echo $number_3 = $_GET['number_3'];
          echo "個";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['tax_3'])){
          echo $tax_3 = $_GET['tax_3'];
         }
          ?>
       </td>
       <td>
         <?php
         if(isset($_GET['price_3'],$_GET['number_3'],$_GET['tax_3'])){
         $nedan_3 = $_GET['price_3'] * $_GET['number_3'] * $_GET['tax_3'];
          echo $nedan_3;
          echo "円（税込み）";
         }
         ?>
       </td>
     </tr>
     <tr>
       <td>
         <?php
         if(isset($_GET['productname_4'])){
          echo $productname_4 = $_GET['productname_4'];
         }
         ?>&nbsp;
       </td>
       <td>
         <?php
         if(isset($_GET['price_4'])){
          echo $price_4 = $_GET['price_4'];
          echo "円（税抜き）";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['number_4'])){
           echo $number_4 = $_GET['number_4'];
           echo "個";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['tax_4'])){
          echo $tax_4 = $_GET['tax_4'];
         }
           ?>
       </td>
       <td>
         <?php
         if(isset($_GET['price_4'],$_GET['number_4'],$_GET['tax_4'])){
         $nedan_4 = $_GET['price_4'] * $_GET['number_4'] * $_GET['tax_4'];
         echo $nedan_4;
         echo "円（税込み）";
         }
         ?>
       </td>
     </tr>
     <tr>
       <td>
         <?php
         if(isset($_GET['productname_5'])){
           echo $productname_5 = $_GET['productname_5'];
         }
         ?>&nbsp;
       </td>
       <td>
         <?php
         if(isset($_GET['price_5'])){
           echo $price_5 = $_GET['price_5'];
           echo "円（税抜き）";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['number_5'])){
           echo $number_5 = $_GET['number_5'];
           echo "個";
         }
         ?>
       </td>
       <td>
         <?php
         if(isset($_GET['tax_5'])){
          echo $tax_5 = $_GET['tax_5'];
         }
           ?>
       </td>
       <td>
         <?php
         if(isset($_GET['price_5'],$_GET['number_5'],$_GET['tax_5'])){
         $nedan_5 = $_GET['price_5'] * $_GET['number_5'] * $_GET['tax_5'];
         echo $nedan_5;
         echo "円（税込み）";
         }
         ?>
       </td>
     </tr>
     <tr>
     <td colspan="4">合計</td><td></td>
   </tr>
   </table>
 </body>
</html>
